#include <stdio.h>
#include <math.h>

int main() {
    double a = 1.0;
    double b = -1.0;
    double c = 0.25;

    double bb4ac = b * b - 4.0 * a * c;

    if (bb4ac < 0) {
        printf("Keine Lösung");
    } else if (bb4ac == 0) {
        double x = (-b + sqrt(bb4ac)) / (2 * a);
        printf("x = %f", x);
    } else {

        double x1 = (-b + sqrt(bb4ac)) / (2 * a);
        double x2 = (-b - sqrt(bb4ac)) / (2 * a);
        printf("x1 = %f\nx2 = %f", x1, x2);
    }
    return 0;
}