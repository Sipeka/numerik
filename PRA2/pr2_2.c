#include <stdio.h>
#include <math.h>

int main() {
    double x;
    double y;
    printf("x eingeben ");
    printf("y eingeben ");
    scanf ("%lf %lf", &x, &y );
    double rho = sqrt(x*x+y*y);
    double phi = 180.0/M_PI*atan(y/x);
    printf ("Übergang von kartesischen zu Polarkoordinaten\n roh = %.10f \n phi = %.10f [Grad]", rho, phi);
    return 0;
}