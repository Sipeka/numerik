#include <stdio.h>
#include <math.h>

int main() {
    double x = M_PI/4.0;

    printf("\trelativer fehler 1 \t relativer Fehler 2 \n");

    for (int i = 1; i < 13 ; i++) {
        double h = pow(10.0, -i);
        /*printf("%f \t", h);*/

        /*double fx1 = exp(x);*/
        double fx2 = exp(-x)*sin(x*x/2.0);

        /*double fxplush1 = exp(x+h);*/
        double fxplush2 = exp(-x-h)*sin((x+h)*(x+h)/2.0);

        /*double fxminush1 = exp(x-h);*/
        double fxminush2 = exp(-x+h)*sin((x-h)*(x-h)/2.0);

        /*double fd1 = (fxplush1-fx1)/h;*/
        double fd2 = (fxplush2-fx2)/h;

        /*double cd1 = (fxplush1-fxminush1)/(2.0*h);*/
        double cd2 = (fxplush2-fxminush2)/(2.0*h);

        /*double analytLsgAbleitung1 = exp(x);*/
        double analytLsgAbleitung2 = -1.*exp(-x)*sin(x*x/2.) + x*exp(-x)*cos(x*x/2.);

        /*double e11 = fabs(fd1-analytLsgAbleitung1)/fabs(analytLsgAbleitung1);*/
        /*double e21 = fabs(cd1-analytLsgAbleitung1)/fabs(analytLsgAbleitung1);*/

        double e12 = fabs(fd2-analytLsgAbleitung2)/fabs(analytLsgAbleitung2);
        double e22 = fabs(cd2-analytLsgAbleitung2)/fabs(analytLsgAbleitung2);


        printf("\t%.10f\t\t %.10f \n", e12, e22);


    }
    return 0;
}
