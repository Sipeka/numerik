//          Füllstandsregelung mit PI-Regler
//          ODE Solver: Explizites Eulerverfahren mit adaptiver Schrittweitensteuerung
//
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<nmrc.h>
#include "gnuplot_i.h" 

extern void f_ein(double **startwerte);

//globale Variablen     können im ganzen programm benutzt werden
int    count = 0;
double A1;
double A2;
double Kp;
double Ki;
double h_soll;
double I_start;
double delta_start;
double h_start;
double dt_min = 0.05; //dt_min = min schrittweite wählen, wenn 0.01 -> numerische fehler addieren sich iwann auf bei kp = 8 und Ki = 0 :(

double *t_vec;      
double *h_vec;       
double *I_vec;       
double *Vzu_vec;     
double *Vab_vec;    
double *dt_vec;     
double *delta_vec;   
double *hsoll_vec;
    
    


double V_zu (double t){
    double V_zu=0;
    if (t >= 1) {
        V_zu = 0.5;
    }
                    //Zulaufvolumenstrom
    return V_zu;
}

void rhs_ode(int n, double t, double *y, double *f) //rechte Seite berechnen  // rhs_ode ist fx funktion, macht magische dinge
{
    double h = y[0];
    double I = y[1];
    double e = h_soll - y[0]; 
    f[0] = 1/A1*(V_zu(t)+(Kp*e+I)*A2*pow(2*9.81*h, 0.5));
    f[1] = Ki*e;
    f[2] = pow(e,2);

} 


double V_ab_berechnung (double h, double I) {
    double e = h_soll - h;
    double y = -(Kp*e+I);
    if (y<0) {
        printf("y ist zu klein.\ny = %lf", y);
        y = 0;
    }
    if (y>1) {
        printf("y ist zu groß.\ny = %lf", y);
        y = 1;
    }
    double V_ab = y*A2*pow((2*9.81*h),0.5);
    
    return V_ab;
}


void out1(int n, double t, double *y, double dt) //Ausgabefunktion; in globale Variablen schreiben // out1 = psol funktion hier
{
   
    double h = y[0];
    double I = y[1];
    
    t_vec[count]       = t;
    h_vec[count]       = h;
    I_vec[count]       = I;
    Vzu_vec[count]     = V_zu(t);
    Vab_vec[count]     = V_ab_berechnung(h, I);
    dt_vec[count]      = dt;
    delta_vec[count]   = y[2];
    hsoll_vec[count]   = h_soll;
    
     count++;
    
}

int euler_exp(int n, double ts, double t_end, double dt, double *y, void(*fx)(), void(*out1)()){
    

    double *F, *y_dach, *y_tilde, *y_12, *Diff;
    int dim;

    /* Speicherallokierung */
    dim = 5*n;

    F       = calloc(dim, sizeof(double));  //in speciherplatz F werden ydach, ytilde... geschrieben
    y_dach  = F+n;
    y_tilde = F+2*n;
    y_12    = F+3*n;
    Diff    = F+4*n;
    double t = ts;
    

    do{
        fx(n, t, y, F);  //F wird verändert
        int i;
        for (i=0; i<n; i+=1) {
            y_tilde[i] = y[i]+dt*F[i];
            y_12[i] = y[i]+dt/2*F[i];   //erster halbschritt
        }
        
        fx(n, t+dt, y_12, F);  //F wird verändert
        double maxnorm = 0;
        for (i=0; i<n; i+=1) {
            y_dach[i] = y_12[i]+dt*F[i];        //2. halbschritt
            
            Diff[i] = fabs(y_tilde[i]-y_dach[i]);   //differenz aller zustände
            maxnorm = max(maxnorm,Diff[i]);     //maxnorm: beide werte in klammer werden verglichen und größerer ist max
        }
        
        double EST = maxnorm/(1-0.5); //0.5 = 2^(-p) mit p = 1  //fehler berechnen
        
        double tau_0 = 0.01;   //genauigkeit wählen
        
        //Speicherung der Werte
        out1(n, t, y, dt);
        
        
        
        if ((EST/dt)<= tau_0 || dt<= dt_min) {
            for (i=0; i<n; i+=1) {
                y[i] = y_dach[i];
            }
            t = t + dt;
            if (t == t_end) {
                break;
            }
            double eta = 1.2;   //vergrößerungsschranke wählen
            double roh = 0.7;  //sicherheitsfaktor für Schrittweitensteuerung wählen
            dt = max(dt_min, min(eta*dt, roh*(tau_0/EST*dt*dt))); //neuer schrittweitenvorschlag
            
            if (t + dt >= t_end) {
               dt = t_end - t; 
            }
        }
        else {
            dt = dt/2;  
        }


    }while(t<t_end+dt);

    free(F);

    return 0;
}

int main(){
    //Festlegen der Variablen
    double *y, *startwerte;
    double t_start, t_end, dt_start;
    int err, n, dim, z;

    //Einlesen der Variablen
    f_ein(&startwerte);
    
    n = startwerte[0];
    t_start = startwerte[1];
    t_end =startwerte[2];
    dt_start = startwerte[3];
    A1 = startwerte[4];
    A2 = startwerte[5];
    Kp = startwerte[6];
    Ki = startwerte[7];
    h_soll = startwerte[8];
    h_start = startwerte[9];
    I_start = startwerte[10];
    delta_start = startwerte[11];
    
    

    //Speicherallokoierung
    z   = (t_end-t_start) / dt_min + 1; //z = obere abschätzung wie häufig die do schleife von euler höchstens läuft
    
    y = calloc(n, sizeof(double));

    dim = n*z + 6*z;
    // Zuweisung von Adressen
    t_vec       = calloc(dim, sizeof(double));
    h_vec       = t_vec + z;
    I_vec       = h_vec + z;
    Vzu_vec     = I_vec + z;
    Vab_vec     = Vzu_vec + z;
    dt_vec      = Vab_vec + z;
    delta_vec   = dt_vec + z;
    hsoll_vec   = delta_vec + z;

    err = euler_exp(n, t_start, t_end, dt_start, y,rhs_ode,out1);
    
    printf("error: %d", err);

    
    //grafische und tabellarische Ausgabe
            
    //grafische darstellung via gnuplot:
     
    gnuplot_ctrl *h;

    h=gnuplot_init();
    
    gnuplot_set_xlabel(h, "zeit t [s]") ;
    gnuplot_set_ylabel(h, "Volstrom") ;
    //gnuplot_plot_xy(h, t_vec, Vzu_vec, count, "Zulaufvolstrom") ;
    //gnuplot_plot_xy(h, t_vec, Vab_vec, count, "Ablaufvolstrom");
    gnuplot_plot_xy(h, t_vec, h_vec, count, "Füllhöhe");
    gnuplot_plot_xy(h, t_vec, delta_vec, count, "delta");
    
    printf("Press Enter to close");
    //scanf("%lf",&weiter);
    
    while (getchar()!='\n'){}
    
    gnuplot_close(h); 
    
            
    // tab Ausgabe:
   //Tabellierte Ausgabe
    FILE *fp=fopen("A23_schreibdatei.dat","w");
    fprintf(fp,"Zeit t [s] \t\t\t Füllhöhe h [m] \t\t\t I-Anteil  \t\t\t Zulaufvolumenstrom\t\t\t Auslaufvolumenstrom\t\t\tZeitschritt dt [s] \t\t\t delta\t\t\t Sollhöhe hsoll [m] \n");
    int i;
    for (i=0;i<count;i+=1)
        {
                     
               fprintf(fp,"%3.5lf\t\t\t\t\t %3.5lf\t\t\t\t\t %3.5lf\t\t\t\t\t%3.5lf\t\t\t\t\t%3.5lf\t\t\t\t\t%3.5lf\t\t\t\t\t%3.5lf\t\t\t\t\t%3.5lf\n",t_vec[i],h_vec[i],I_vec[i],Vzu_vec[i],Vab_vec[i],dt_vec[i],delta_vec[i],hsoll_vec[i]);  
    }
    
    
    
    

    return 0;
}

 
