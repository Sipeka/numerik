#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<nmrc.h>


//void f_ein(int *neq, double *tstart,double *tend, double *dt, double **yi){
void f_ein (double **startwerte) { // ** weil man die startwerte hier einliest und später wieder verwendet und es viele sind

    int n = 12, i; // anzahl variablen in para.dat

    //Dateiname Abfragen

    FILE *fd;
    
    //Datei öffnen

    //Fehler beim Einlesen?
    if ((fd = fopen("para.dat", "r"))== NULL)
    {fprintf (stderr, "Datei existiert nicht!\n");
        exit(1);
    }

    //Einlesen

    //Speicherallokierung für startwerte-Vektor
    (*startwerte) = calloc(n, sizeof(double));

    /* Einlesen der Anfangsbedingungen */
    for (i = 0; i < n; i++) {
        fscanf(fd, "%lf%*[^\n]", (*startwerte)+i);
    }

    fclose(fd);

}


 
