#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dirent.h>

double f(double x) {
    double A = exp(16.7+91.5*x);
    double E = 28.4+135.5*x;
    double T = 713.15;
    double R = 0.001987;
    double t = 80.;
    double erg = -A*exp(-E/R/T)*t-log(1-x);
    return erg;
}

int bisekt(double tol, double a, double b, double (*f)(double), double *xs) { // stern bei xs weil ich das später ausgeben will, tol ist toleranz zur nullstelle
    double fa = f(a); // fa ist f an stelle a
/* Test. Ist [a,b] ein eingrenzendes Intervall?  Dh a und b angeben und wenn ein wert neg ist und der andere pos, wird NS gesucht*/
    if (fa * f(b) > 0) {
        return -1; /* Fehler , wenn -1 kommt gehts nicht weiter*/
    }
    int sgn = fa > 0 ? 1 : -1; /*Vorzeichen von f(a) frage: ist fa größer 0? wenn ja, dann vorzeichen sgn = 1 wenn nicht dann -1 */
    do {
        /* Halbiere das Intervall x = (a+b)/2 */
        double x = (a + b) / 2;

        double fx = f(x);
        /* Suche ein neues eingrenzendes Intervall */
        if (sgn * fx > 0) {  //f(x) ist ein wert der funktion f
            a = x; /* Suchintervall ist [x,b] */
            b = b;
        }
        else {
            a = a; /* Suchintervall ist [a,x] */
            b = x;
        }

        if (isnan(fx)) {
            return -1;
        }
    }
    while (fabs(a - b) > tol);  // do hört auf wenn hier a-b <= tol steht
    *xs = (a + b) / 2; /* gefundene Nullstelle */
    return 0; /* Erfolg */

}

int main() {
    double xs;
    double tol = 0.0000001;
    double a = -1;
    double b = 1;
    int Erfolg = bisekt(tol, a, b, f, &xs); //&xs ist speicheradresse wo 0 steht, die wird oben benutzt und der wert für xs reingeschrieben
    printf("Erfolgreiche Suche? (Ja: 0; Nein: -1): %d\n", Erfolg);
    printf("Nullstelle: %lf", xs);

}