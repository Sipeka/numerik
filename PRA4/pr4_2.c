#include <stdio.h>
#include <math.h>

//erste Ableitung von Hand gerechnet
double ersteAbleitung(double r) {
    double V = 150;
    return 4*M_PI*r-2*V*pow(r,-2);
}

// Ableitung an der Stelle x berechnen (dh. hier 2. Ableitung an Stelle r)
double Ableitung(double (*f)(double), double x) {
    double h = pow(10, -3);
    return (f(x+h) - f(x)) / h;
}
// so lange bis f'(r) kleiner 0.0001
double Newtonverf(double (*ersteAbleitung)(double), double r) {
    while (fabs(ersteAbleitung(r)) >= 0.0001) {
        r = r - ersteAbleitung(r) / Ableitung(ersteAbleitung, r);
    }
    return r;
}

int main() {
    double r1 = 3; //random startwert für den radius r
    double ropt = Newtonverf(ersteAbleitung, r1);
    printf("ropt = %f\n", ropt);
    printf("hopt = %f\n", 150./M_PI/pow(ropt,2));
    printf("F(ropt) = %f", 2*M_PI*pow(ropt,2)+2*M_PI*ropt*150./M_PI/pow(ropt,2));
};