#include <stdio.h>
#include <nmrc.h>
#include <stdlib.h>
#include <math.h>


void printMatrix(int dim, double *matrixvec, double *b) {
    printf("\n");
    int breite = dim;
    int zeile, spalte;
    for(zeile = 0; zeile < dim; zeile++) {
        for(spalte = 0; spalte < dim; spalte ++) {
            
            
            printf(" %f ", matrixvec[zeile*dim+spalte]);
        } 
        printf("%f\n", b[zeile]);
    }
}
 
 // TEIL 2 Gaußelimination
    
    // ldc,Cstern,ldc,ystern,lg
int gauss_elim(int dim, double *matrixvec, int fdim, double *b, double *x) {
    int breite = dim;
    int zeile, spalte;
         
    printf("gauss dim: %d, fdim: %d", dim, fdim);
         
    int k, n;
    
    for ( k=0;k < dim-1;k++){ 
            
        double max_Pivotelement = matrixvec[k*breite+k];
        int max_zeilePivotelement = k;
        
            
   //Pivotsuche:
        for ( zeile = k; zeile<(dim); zeile+=1) {
    
            if (fabs(max_Pivotelement)<fabs(matrixvec[zeile*breite+k])) {
                max_Pivotelement = fabs(matrixvec[zeile*breite+k]);  
                max_zeilePivotelement = zeile;
            }
            
        }
        printf(" max_zeilePivotelement: %i", max_zeilePivotelement);
                
              // zeile = max_StellePivotelement/(dim+1) //zeile in der das größte pivotelement steht
        printMatrix( dim,  matrixvec,  b)  ;
                
   //ZEILENVERTAUSCHUNG :             
             //stelle an der die vertauschung der zeilen beginnt
            
        if (max_zeilePivotelement !=  k) {
            
            double alterwertvector = b[k];
            b[k]= b[max_zeilePivotelement];
            b[max_zeilePivotelement] = alterwertvector;
            
            // for ( int spalte = 0; spalte <= dim; spalte +=1) {
            for ( spalte = 0; spalte < dim; spalte +=1) {   
                
                
                double alterwert = matrixvec[max_zeilePivotelement*breite + spalte];
                matrixvec[max_zeilePivotelement*breite + spalte] = matrixvec[k*breite+spalte];
                matrixvec[k*breite+spalte] = alterwert;
                
            }
        }
        printMatrix( dim,  matrixvec,  b)  ;
                
    //Aufdatierung:
         // k = 0: in nullte spalte nuller schreiben, k = 1: in erste spalte nuller schreiben...
         // breite = dim überall
         
        for ( zeile=k+1;zeile<dim;zeile++){
            
            for ( spalte=k+1;spalte<dim;spalte++) { //endmatrix hat keine nullen unten links, weil sont schon mit der null weitergerechnet werden würde und das wäre falsch, aber die anderen zahlen stimmen
                
                matrixvec[zeile*breite+spalte] = matrixvec[zeile*breite+spalte]*matrixvec[k*breite+k] - matrixvec[zeile*breite+k] *  matrixvec[k*breite+spalte];
            
            }
            b[zeile]=b[zeile]*matrixvec[k*breite+k]-matrixvec[zeile*breite+k]*b[k];
       
        }
        
                    
                    
            
           
    }
        
   //Rückwärtssubstitution:
            
            
    for (zeile = dim-1; zeile>= 0; zeile -=1){
        int z0; // z0 = nach oben laufen in matrix
        x[zeile] = b[zeile]/matrixvec[zeile*breite+zeile];
        for (z0 = zeile - 1; z0 >= 0; z0 -=1) {
            b[z0] = b[z0] - x[zeile]*matrixvec[z0*breite+zeile];
        }
        
    }
    

    printMatrix( dim,  matrixvec,  b)  ;    
          
    vec_print("\nx vektor: ", " %8.3f ", stdout, dim, dim, x); //x ausgeben 
    
    
    
    return 0; 
    
        
}
 
