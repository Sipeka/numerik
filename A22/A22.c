#include <stdio.h>
#include <nmrc.h>
#include <stdlib.h>
#include <math.h>
#include <gnuplot_i.h>
extern int gauss_elim(int dim, double *matrixvec, int fdim, double *b, double *x);
//gcc A22.c -o A22 -g -I$NMRC_PREFIX/include -I/stud/usnm4/src/gnuplot_i/src -L$NMRC_LIB -L /stud/usnm4/src/gnuplot_i/src -lnmrc -lm 



void einlesen(int *AnzahlWertepaare, int *AnzahlParameter) {  //  * nur, wenn ich die werte am ende raushaben will auf funktion. kein stern, wenn ich die werte brauch aber sie nicht am ende der funktion will

    FILE *fdMessdaten;
    
    fdMessdaten = fopen("Messdaten","r");
    
    fscanf(fdMessdaten, "%d %d", AnzahlWertepaare, AnzahlParameter);
    
    fclose(fdMessdaten); //datei schließen
    
    printf("Anzahl Messdaten: %d \nAnzahl Parameter: %d\n",*AnzahlWertepaare, *AnzahlParameter); 
    
}

// funktion um matrix auszugeben
void printvector(int AnzahlWertepaare, int AnzahlParameter, double *C) {
    printf("\n");
    
    int zeile, spalte;
    for(zeile = 0; zeile < AnzahlWertepaare; zeile++) {  //nach unten laufen
        for(spalte = 0; spalte < AnzahlParameter; spalte ++) {  //nach rechts laufen
            
            
            printf(" %lf ", C[zeile*AnzahlParameter+spalte]);
        
        }
        printf(" \n" );
    }
}


double * matrixbelegung(int AnzahlWertepaare,int AnzahlParameter) {  //wenn ich matrix returne, muss am anfang der fkt double* (bzw wenn es den pointer zurückgibt)
    
    double *T, *P, *C, *y, *ergebnis;
    int zeile;
    
    FILE *fdMessdaten;
    
    fdMessdaten = fopen("Messdaten","r");
    
    fscanf(fdMessdaten, "%*[^\n]\n"); //überspringt erste zeile
    
    // vec_alloc(&y, sizeof(*y), AnzahlWertepaare);
    // mat_alloc(&C,sizeof(*C),AnzahlWertepaare,AnzahlParameter);
        
    C = (double*)calloc(AnzahlWertepaare*AnzahlParameter,sizeof(double)); //speicher bereitmachen matrix c
    y = (double*)calloc(AnzahlWertepaare,sizeof(double)); //speicher bereitmachen für y
    T= (double*)calloc(AnzahlWertepaare,sizeof(double));
    P= (double*)calloc(AnzahlWertepaare,sizeof(double));
    ergebnis = (double*)calloc(AnzahlParameter,sizeof(double));
     // double *ergebnis;
     //vec_alloc(&ergebnis, sizeof(*ergebnis), AnzahlParameter);
      
    for (zeile = 0; zeile<AnzahlWertepaare; zeile +=1){
        
        fscanf(fdMessdaten, "%lf %lf", &T[zeile], &P[zeile]); // springt nach lesen automatisch in nächsste zeile
        double x = 1./T[zeile];
       //printf("T: %lf", T);
        //printf("P: %lf \n", P);
         y[zeile] = log(P[zeile]);
        double f0 = 1.;
        double f1 = x;
        double f2 = x*y[zeile];
        C[0+zeile*AnzahlParameter] = f0; //belegung der matrix mittels forschleife
         C[1+zeile*AnzahlParameter] = f1;
          C[2+zeile*AnzahlParameter] = f2;
        
          //printf("y[zeile]= %lf\n", y[zeile]);
    }
    
    fclose(fdMessdaten); //datei schließen
    

   // printvector(AnzahlWertepaare, AnzahlParameter, C);
    
    //QR zerlegung:
    // vec_alloc(&ergebnis, sizeof(*ergebnis), AnzahlWertepaare);
    //printf("n: %d m: %d ldc: %d\n", AnzahlWertepaare, AnzahlParameter, AnzahlParameter);
    double error = nmrc_lapqr(AnzahlWertepaare, AnzahlParameter, C, AnzahlParameter, y, ergebnis);
    
     double A=ergebnis[0];
    double c=-1*ergebnis[2];
    double B=A*c-ergebnis[1];
            
            
    //printvector(1, AnzahlParameter, ergebnis);
    
    printf("A, B, C, = %lf %lf %lf", A, B, c);
    
        //p Berechnung
    double *p, *delta;
    
            vec_alloc(&p, sizeof(*p), AnzahlWertepaare);
            
            vec_alloc(&delta, sizeof(*delta), AnzahlWertepaare);
            for(zeile=0;zeile<AnzahlWertepaare;zeile+=1)
            {
                p[zeile]=exp(A-B/(T[zeile]+c));
                
            printf("\np[%d]=%lf ",zeile,p[zeile]);
                
            }
                
            // Gesamtfehler
            double fehler=0;
            for(zeile=0;zeile<AnzahlWertepaare;zeile+=1)
            { 
                delta[zeile]=p[zeile]-P[zeile];
                fehler=fehler+delta[zeile]*delta[zeile];
            }
            printf("\nGesamtfehler Delta:%lf\n",fehler);
            
            
            

      //Tabellierte Ausgabe machen
            FILE *fp=fopen("Antoine.txt","w");  //abtoine datei wird erstellt und beschrieben
            fprintf(fp,"Temperatur T in [K] \t\t\t Antoinedruck in [kPa] \t\t\t gemessener Druck in [kPa] \t\t\t Fehler/Differenz\n");
            for (zeile=0;zeile<AnzahlWertepaare;zeile+=1)
            {
                        
                fprintf(fp,"%3.2lf                        %3.5lf                               %3.5lf                            %1.5lf\n",T[zeile],p[zeile],P[zeile],delta[zeile]);
                    
                
            }
            
            
            
    //grafische darstellung via gnuplot
     
            gnuplot_ctrl *h;
        
            h=gnuplot_init();
            
            gnuplot_set_xlabel(h, "Temperatur T [K]") ;
            gnuplot_set_ylabel(h, "Druck P [kPa]") ;
            gnuplot_plot_xy(h, T, p, AnzahlWertepaare, "berechneter Druck") ;
            gnuplot_plot_xy(h, T, P, AnzahlWertepaare, "gemessener Druck");
            
            printf("Press Enter to close");
            //scanf("%lf",&weiter);
            
            while (getchar()!='\n'){}
            
            gnuplot_close(h); 
    
            
            
            
            
            
            
            
            
            //Gauss mit Normalengls
        
            //Allokierung Speicher
            double *ystern, *sstern, *Ctrans, *Cstern, *lg;
            int ldc = AnzahlParameter;
            
            
            vec_alloc(&ystern, sizeof(*ystern), AnzahlWertepaare);
            mat_alloc(&Ctrans,sizeof(*Ctrans),AnzahlParameter,AnzahlWertepaare);
            mat_alloc(&Cstern,sizeof(*Cstern),AnzahlWertepaare,AnzahlParameter);
            int ldt=AnzahlWertepaare;
            int ldy=1;
            nmrc_trans(AnzahlWertepaare,AnzahlParameter,C,ldc,Ctrans,ldt);            
           
             nmrc_mmult(AnzahlParameter,AnzahlWertepaare,AnzahlParameter,Ctrans,ldt,C,ldc,Cstern,ldc);
            nmrc_mmult(AnzahlParameter,AnzahlWertepaare,ldy,Ctrans,ldt,y,ldy,ystern,ldy);
            
            /* Ausgabe von C
            printf("\n y stern=\n");
            for(i = 0; i < m; i++) {
                                printf("%lf  ", y_stern[i]);
                    printf("\n");
            } */
        
            
        // Lösung LGS mit Gauss
        vec_alloc(&lg, sizeof(*lg), AnzahlParameter);

        //int dim, double *matrixvec, int fdim, double *b, double *x
       
        
        printvector( AnzahlParameter, AnzahlParameter, Cstern);
         printvector( AnzahlParameter, 1, ystern);
        printf("ldc: %d", ldc);
       
        
        gauss_elim (ldc,Cstern,ldc,ystern,lg);
         
        //nmrc_gauss(m,C_stern, m,y_stern,l);
        
        /*printf("\n x =\n");
            for(i = 0; i < m; i++) {
                    printf("%lf  ", l[i]);
                    printf("\n");
            } */
                
        //gauss_elim (m,C_stern,ldc,y_stern,x);
        
            
            // Berechnung und Ausgabe der Modellparameter aus x
            double A_gauss=lg[0];
           double c_gauss=-1*lg[2];
           double B_gauss=A_gauss*c_gauss-lg[1];
            
            // Ausgabe Modellparameter 
            printf("\nDie Modellparameter der Antoine Gleichung mit Gauss lauten:\nA:%lf\nB:%lf\nC:%lf\n",A_gauss,B_gauss,c_gauss);
            
       
        printf("\n Zum Beenden STRG + C druecken");
        printf("\n Zum Beenden STRG + C druecken\n");  
        
         
      
         
                
            
    
    
    
    return C;
}



int main () {
    
    int AnzahlWertepaare, AnzahlParameter;
    
    einlesen(&AnzahlWertepaare, &AnzahlParameter);
    matrixbelegung(AnzahlWertepaare, AnzahlParameter);
    
    
    
    return 0;
}
