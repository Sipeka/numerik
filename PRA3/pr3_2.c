#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dirent.h>

int main() {
    int n;
    int m;
    //n und m eingeben und prüfen, ob n oder m zu groß
    printf("Anzahl der Zeilen (n) und Anzahl der Spalten (m) eingeben ");
    scanf ("%d %d", &n, &m);
    if (n > 20 || m > 50) {
        printf("Fehler! Dimension n bzw m kleiner wählen");
        return 0;
    }

    //Matrix A berechnen und speicher reservieren
    double *Matrix;
    Matrix = calloc(n*m, sizeof(double));
    double MaxNorm = 0;
    double Frobeniusnorm = 0;

    for(int i = 0; i <= n - 1; i++) { //zeilen berechnung
        double sumMNorm = 0; // addition der werte der einzelnen zeilen
        for (int j = 0; j <= m - 1; j++) { //spalten berechnung
            Matrix[i*m+j] = (1.0 + i * j) / (1.0 + i + j);
            Frobeniusnorm += (pow(Matrix[i*m+j],2));
            sumMNorm += fabs(Matrix[i*m+j]);
        }

        MaxNorm = fmax(sumMNorm, MaxNorm);
    }
    printf("Frobeniusnorm = %lf", sqrt(Frobeniusnorm));
    printf("\nMaximumnorm = %lf", MaxNorm);

    return 0;
}