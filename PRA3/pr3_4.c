#include "newton.h"
#include <stdio.h>
#include <math.h>

//funktion definieren
double fDreieck(double x) {
    return (x+3.)/2.*sqrt(3.*x) - 20.;
}

int main() {
    double x = 1.; //random startwert
    x = Newtonverf(fDreieck, x);
    //printf("%lf", x);

    double A = 20.;
    double d = 3.;
    double h = sqrt(x * d);
    double c = x + d;
    double b = sqrt(x*x + h*h);
    double a = sqrt(d*d + h*h);

    printf ("a = %.10lf\nb = %.10lf\nc = %lf", a, b, c);
}

