#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dirent.h>


int main() {
    int n;
    double x;
    printf("Hier Grad n und Stelle x, an der ausgewertet werden soll durch Leerzeichen getrennt eingeben");
    scanf("%i %lf", &n, &x);

    double *a = calloc(n + 1, sizeof(double)); //Länge vom Array ist n + 1, dh fängt bei 0 an und geht bis n

    printf("Hier n+1 Koeffizienten aj durch Leerzeichen getrennt eingeben");
    for (int i = n; i >= 0; i--) {
        scanf("%lf", &a[i]);

    }
    double s = a[n];
    for (int j = n - 1; j >= 0; j--) {
        s = s * x + a[j];
    }
    double Ergebnis = s;
            printf("Ergebnis: %lf", Ergebnis);
}