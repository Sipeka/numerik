#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dirent.h>

// Ableitung berechnen
double Ableitung(double (*f)(double), double x) {
    double h = pow(10, -5);
    return (f(x+h) - f(x)) / h;
}
// 0 stelle berechnen, k bis große zahl, große zahl = schritte wie oft es sucht nach 0, * weil f(x) weiter unten funktion (nicht mathematisch) ist
double Newtonverf(double (*f)(double), double x) {
    for (int k = 1; k < 10000; k++) {
        x = x - f(x) / Ableitung(f, x);
    }
    return x;
}