#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dirent.h>


// 0 0; 1 0.84147; 2 0.90929; 3 0.14112; 4 -0.7568;

int main() {
    int Anzahl_Stutzstellen;
    printf("Anzahl der Stützstellen eingeben");
    scanf("%i", &Anzahl_Stutzstellen);
    printf("Stützstellen in Form \"x y; \" eingeben");
    //scanf("%i %lf", &n, &x);

    double *x = calloc(Anzahl_Stutzstellen, sizeof(double));
    double *y = calloc(Anzahl_Stutzstellen, sizeof(double));

    for (int i = 0; i <= Anzahl_Stutzstellen - 1; i++) {
        scanf("%lf %lf;", &x[i], &y[i]);
    }

    double Stelle;
    printf("Stelle x eingeben, an der das Polynom ausgewertet wird (x muss zwischen der ersten und letzten Stützstelle liegen)");
    scanf("%lf", &Stelle);

    // Berechnung von Li:
    int i;
    double P = 0;

    for (i = 0; i <= Anzahl_Stutzstellen - 1; i++) { //Äußere Schlewife: Summe
        double Li = 1;


        for (int j = 0; j <= Anzahl_Stutzstellen - 1; j++) {
            if (j != i) {

                Li *= (Stelle - x[j]) / (x[i] - x[j]); // Innere Schleife: Multiplikation

            }
        }
        P += y[i] * Li; //zu Äußere Schleife: Summe
    }
    printf("Ergebnis: %lf\n", P);

}
