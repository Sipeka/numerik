#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dirent.h>

int main() {
    // pointer der auf matrix txt datei zeigt erstellen fp = matrixfilepointer
    FILE *matrixfilepointer;

    //sagen wie matrix file heißt
    char DateinameMatrix[100];
    printf("Hier Dateiname mit Matrix eingeben ");
    scanf("%s", DateinameMatrix);

//  sagen wie vector file heißt
    char DateinameVec[100];
    printf("Hier Dateiname mit Vector eingeben ");
    scanf("%s", DateinameVec);

    //sagen wie ausgabefile heißen soll
    char DateinameAusgabefile[100];
    printf("Hier Dateiname der Ausgabedatei eingeben ");
    scanf("%s", DateinameAusgabefile);


    //matrixfilepointer = fopen("../PRA3/MatrixA.txt","r");
    matrixfilepointer = fopen(DateinameMatrix, "r");

    // schauen, ob es datei an der stelle gibt mit NULL heißt der pointer zeigt nirgends hin
    if (matrixfilepointer == NULL) {
        printf("Matrix not found, %s", DateinameMatrix);
        return 0;
    }
    //dimension aus matrix datei auslesen
    int zeilen;
    int spalten;
    fscanf(matrixfilepointer, "dim = %dx%d", &zeilen, &spalten);

    //rausfinden, wie viele kästchen ich brauch im speicher
    int kaestchemimspeicher_matrix = zeilen * spalten;

    //allokieren: sizeof heißt anzeil der bytes pro datentyp (hier double), brauch ich weil ich die reservieren will und platz merken, wo speicher für matrixwerte grad reserviert wurde = matrixspeicherplatz ist ein pointer
    double *matrixspeicherplatz = malloc(kaestchemimspeicher_matrix * sizeof(double));

    //matrix aus datei einlesen. erste zeile wurde schon weiter oben gelesen (dim), deshlab les ich auromatisch in zeile 2 weiter und da fängt matrix an
    for (int laufvar = 0; laufvar < zeilen * spalten; laufvar++) {
        double *adressevonjedemeinzelenkaestle_matrix = matrixspeicherplatz + laufvar;
        //ich will das scannen wo pointer draufzeigt
        fscanf(matrixfilepointer, "%lf", adressevonjedemeinzelenkaestle_matrix);
    }

    // gibt mir die einzelnen werte der matrix aus
    for (int laufvar = 0; laufvar < zeilen * spalten; laufvar++) {
        printf("zahl %d: %lf\n", laufvar, *(matrixspeicherplatz + laufvar));
    }


    // ******** VEKTOR********//

    // pointer der auf vec txt datei zeigt erstellen fp = vecfilepointer
    FILE *vecfilepointer;

    // open vec file
    vecfilepointer = fopen(DateinameVec, "r");

    // schauen, ob es datei an der stelle gibt mit NULL heißt der pointer zeigt nirgends hin
    if (vecfilepointer == NULL) {
        printf("Vector not found");
        return 0;
    }
    //dimension aus vec datei auslesen
    int zeilenvec;
    fscanf(vecfilepointer, "dim = %d", &zeilenvec);


    //rausfinden, wie viele kästchen ich brauch im speicher
    int kaestchemimspeicher_vec = zeilenvec;

    //allokieren: sizeof heißt anzahl der bytes pro datentyp (hier double), brauch ich weil ich die reservieren will und platz merken, wo speicher für vecwerte grad reserviert wurde = matrixspeicherplatz ist ein pointer
    double *vecspeicherplatz = malloc(kaestchemimspeicher_vec * sizeof(double));

    //vec aus datei einlesen. erste zeile wurde schon weiter oben gelesen (dim), deshlab les ich auromatisch in zeile 2 weiter und da fängt matrix an
    for (int laufvarvec = 0; laufvarvec < zeilenvec; laufvarvec++) {
        double *adressevonjedemeinzelenkaestle_vec = vecspeicherplatz + laufvarvec;
        //ich will das scannen wo pointer draufzeigt
        fscanf(vecfilepointer, "%lf", adressevonjedemeinzelenkaestle_vec);
    }

    // gibt mir die einzelnen werte der vec aus
    for (int laufvarvec = 0; laufvarvec < zeilenvec; laufvarvec++) {
        printf("zahlvec %d: %lf\n", laufvarvec, *(vecspeicherplatz + laufvarvec));
    }


    //öffnen der ausgabedatei

    FILE *fp;
    fp = fopen(DateinameAusgabefile, "w");

    // write vector in ausgabedatei
    fprintf(fp, "Vektor:\n");

            //vec aus datei einlesen. erste zeile wurde schon weiter oben gelesen (dim), deshlab les ich auromatisch in zeile 2 weiter und da fängt matrix an
            for (int laufvarvec = 0; laufvarvec < zeilenvec; laufvarvec++) {
                double *adressevonjedemeinzelenkaestle_vec = vecspeicherplatz + laufvarvec;
                fprintf(fp, "%lf ", *adressevonjedemeinzelenkaestle_vec);
            }

    // write Matrix in ausgabedatei
    fprintf(fp, "\nMatrix:\n");
    for (int z = 0; z < zeilen; z++) {

        for (int s = 0; s < spalten; s++) {
            fprintf(fp, "%lf ", *(matrixspeicherplatz + s + z * spalten));
        }
        fprintf(fp, "\n");
    }


    //Berechnug von A*x = b und in datei schreiben
    fprintf(fp, "Ergebnisvektor:\n");
    for (int z = 0; z < zeilen; z++) {

        double sum = 0;
        for (int s = 0; s < spalten; s++) {
            sum = sum + *(matrixspeicherplatz + s + z * spalten) * *(vecspeicherplatz + s);

        }


        fprintf(fp, "%lf\n", sum);
    }



    fclose(fp);

    // clear memory for A
    free(matrixspeicherplatz);
    free(vecspeicherplatz);
    fclose(matrixfilepointer);
    fclose(vecfilepointer);
    // A=NULL;

    return EXIT_SUCCESS;
}
