#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dirent.h>
#include "newton.h"


//funktion definieren
double f(double x) {
    return 10. * exp(-3. * x) + 2. * exp(-5. * x) - 6.;
}
// führt sachen aus
int main() {
    double x = -8.; //random startwert
    double nullstelle = Newtonverf(f, x);
    printf("%lf", nullstelle);

}