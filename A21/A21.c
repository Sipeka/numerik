#include <stdio.h>
#include <nmrc.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "gnuplot_i.h"


 
// funktion um matrix auszugeben
    void printvector(int dim, double *matrixvec, double *b) {
        printf("\n");
         int breite = dim;
         int zeile, spalte;
         for(zeile = 0; zeile < dim; zeile++) {
             for(spalte = 0; spalte < dim; spalte++) {
                 
                 
                 printf(" %f ", matrixvec[zeile*dim+spalte]);
             } 
             printf("%f\n", b[zeile]);
         }
        }
    
    
    // TEIL 2 Gaußelimination
    
    
    int gauss_elim(int dim, double *matrixvec, int fdim, double *b, double *x){
        int breite = dim;
         int zeile, spalte;
         
         
    int k, n;
    
        for ( k=0;k < dim-1;k++){ 
            
            double max_Pivotelement = matrixvec[k*breite+k];
        int max_zeilePivotelement = k;
        
            
   //Pivotsuche:
            for ( zeile = k; zeile<(dim); zeile+=1) {
        
                if (fabs(max_Pivotelement)<fabs(matrixvec[zeile*breite+k])) {
                    max_Pivotelement = fabs(matrixvec[zeile*breite+k]);  
                    max_zeilePivotelement = zeile;
                }
                
            }
           // printf(" max_zeilePivotelement: %i", max_zeilePivotelement);
                
              // zeile = max_StellePivotelement/(dim+1) //zeile in der das größte pivotelement steht
             
                
            
              //  printvector( dim,  matrixvec,  b)  ;
                
   //ZEILENVERTAUSCHUNG :             
             //stelle an der die vertauschung der zeilen beginnt
            
            if (max_zeilePivotelement !=  k) {
                
                double alterwertvector = b[k];
                b[k]= b[max_zeilePivotelement];
                b[max_zeilePivotelement] = alterwertvector;
                
                // for ( int spalte = 0; spalte <= dim; spalte +=1) {
                for ( spalte = 0; spalte < dim; spalte +=1) {   
                  
                   
                    double alterwert = matrixvec[max_zeilePivotelement*breite + spalte];
                    matrixvec[max_zeilePivotelement*breite + spalte] = matrixvec[k*breite+spalte];
                    matrixvec[k*breite+spalte] = alterwert;
                    
                    }
        
                    
                    
                }
                
              //  printvector( dim,  matrixvec,  b)  ;
                
    //Aufdatierung:
         // k = 0: in nullte spalte nuller schreiben, k = 1: in erste spalte nuller schreiben...
         // breite = dim überall
         
        for ( zeile=k+1;zeile<dim;zeile++){
            
        for ( spalte=k+1;spalte<dim;spalte++) { //endmatrix hat keine nullen unten links, weil sont schon mit der null weitergerechnet werden würde und das wäre falsch, aber die anderen zahlen stimmen
            
            matrixvec[zeile*breite+spalte] = matrixvec[zeile*breite+spalte]*matrixvec[k*breite+k] - matrixvec[zeile*breite+k] *  matrixvec[k*breite+spalte];
        
        }
    b[zeile]=b[zeile]*matrixvec[k*breite+k]-matrixvec[zeile*breite+k]*b[k];
       
    }
        
                    
                    
            
           
        }
        
   //Rückwärtssubstitution:
            
            
            for (zeile = dim-1; zeile>= 0; zeile -=1){
                int z0; // z0 = nach oben laufen in matrix
                x[zeile] = b[zeile]/matrixvec[zeile*breite+zeile];
                for (z0 = zeile - 1; z0 >= 0; z0 -=1) {
                    b[z0] = b[z0] - x[zeile]*matrixvec[z0*breite+zeile];
                }
                
            }
    

        // printvector( dim,  matrixvec,  b)  ;    
          
    //vec_print("\nx vektor: ", " %8.3f ", stdout, dim, dim, x); //x ausgeben 
    
    
    
    return 0; 
       
            
    }

    

 int main() {
     
     
     int dim;
     double *matrixvec;
     FILE *fd;
     
     
     fd = fopen("A.txt","r"); //Matrix A öffnen
     
     if (read1D_LGS(fd, &dim, &matrixvec) < 0) //LGS (matrix und vektor b) einlesen und in matrixvec schreiben   ... befehl schreibt erst matrix a komplett, dann am ende vektor b
         return 1; //wenn einlesen nicht funktioniert, wird 1 ausgegeben
         
    fclose(fd); //datei schließen
    //printf("%i",dim);
     

    
    
    //berechung konditionszahl
    int fdim = dim; //fdim = führende dim der matrix
    double konzahl = nmrc_condA(dim, matrixvec, fdim, NmrcNormRS); //kondzahl nach maximumnorm berechnen
    //printf("Koonditionszahl: %lf", konzahl);
    
    
    
    // GLS lösen
    //1. speicher allokieren
    int *pv = (int*)calloc(dim, sizeof(int)); //pv = Permutationsvek
    //double *x = (double*)calloc(dim, sizeof(double)); // x = Lösungsvektor
    
    //2. LU zerlegung 
    //int LU = nmrc_ludec(dim, matrixvec, fdim, pv);  //LU zerlegung mit ludec befehl
    
    //3. GLS mit lusol befehl lösen, gesucht: x vector
    double *b = matrixvec + dim*dim; //ab der stelle fängt der b vektor an
    
    //nmrc_lusol(dim, matrixvec, fdim, pv, b, x); 
    
    //vec_print("\nx vektor: ", " %8.3f ", stdout, dim, dim, x); //x ausgeben 
    
    
    //was bei unterschiedlichen b vektoren auffällt: schon kleine veränderungen führen zu starken änderungen des x vektors.
    
    
    // gauss_elim( dim,  matrixvec,  fdim,  b,  x);
    
    
    
    
    
    
     // TEIL 3 
    
    //Matrix A mit random zahlen
    
    double *A_rand, *b_rand;
    

        int n=2500;     //größe des Allokierten speichers auf max. Matrixgröße festlegen-->für alle Matrizen nur 1x allokieren, effizient!
        double *x = (double*)calloc(n,sizeof(double));
        A_rand = (double*)calloc(n*n,sizeof(double));  //gauss
        
        double* a  = (double*)calloc(n*n,sizeof(double));  //LU zerl
        
        
        b_rand = (double*)calloc(n,sizeof(double));  //Gauss
        
        double *B = (double*)calloc(n,sizeof(double));  //LU zerl
        
        
        // Allokierung von pv
        
            vec_alloc(&pv, sizeof(*pv), n);  //LU

       

        

        int n_vec[6] = {200,500,1000,1500,2000,2500};

        int i;
        double dt_vec_gauss[6]; //da 6 elemente in n_vec
        double dt_vec_LU[6]; //da 6 elemente in n_vec
        
        for(i=0;i<=5;i++){

            //Erzeugung der random Matrix A

            nmrc_randmat_gen(n_vec[i],A_rand,n_vec[i],-5.0,5.0);
            
            //A in a kopieren
           int j;
            for (j = 0; j< n_vec[i]*n_vec[i]; j+=1) {
            a[j] = A_rand[j]; //kapiert A matrix in a, da sie sonst nach gauss weg ist aber man sie noch für LU zerlegung braucht
            }
            

            //mat_print("A", "%.5lf", stdout, n,n,n,A);

           // printf("\n");

            nmrc_rsum(n_vec[i],n_vec[i],A_rand,n_vec[i],b_rand);

            // vec_print("b", "%.5lf", stdout, n,1,b);

            // printf("\n");

       
            //b in B kopieren
            for (j = 0; j< n_vec[i]; j+=1) {
            B[j] = b_rand[j]; 
            }
            
            
            

        //starten der Zeitmessung

            double start_gauss = clock();

        
 

        // Gauß-Elimination und LU zerlegung 


        n = n_vec[i];

        printf("n: %d", n);
        gauss_elim(n_vec[i], A_rand,  n_vec[i], b_rand, x);
        
        // Zeitmessung beenden 

        double dt_gauss = clock() - start_gauss;
        
        
        //starten der Zeitmessung

            double start_LU = clock();  //LU
      
        
        nmrc_condA(n, a, n_vec[i],NmrcNormRS); //LU
             
     
         // Lösung LGS
            //LU Zerlegung
            int err=nmrc_ludec(n_vec[i],a,n_vec[i],pv);
            if (err<0){
                printf("Fehler %d\n",err);
                return 1;}
                
            // Berechnung x
            //clclw
            nmrc_lusol(n,a,n_vec[i],pv,B,x);
       

        // Zeitmessung beenden 

        
          double dt_LU = clock() - start_LU; //LU


       

       // Zeit Umwandlen in Sekunden 

            dt_gauss /= CLOCKS_PER_SEC;
    
        dt_vec_gauss[i] = dt_gauss;
        
        
             dt_LU /= CLOCKS_PER_SEC;  //LU
    
        dt_vec_LU[i] = dt_LU;  //LU

       

        //Lösungsvektor

   //    vec_print("lösung x", "%8.3f", stdout, n, n, x);

        

        //Rechenzeit

        printf("Rechenzeit für n=%d: %lf s\n",n_vec[i],dt_vec_gauss[i]);


        }
        
        
        //
        
        
        


        //Plotten mit Gnuplot

        
       gnuplot_ctrl *h1;


           double n_vec_doub[6] = {200.,500.,1000.,1500.,2000.,2500.}; //n-vec als double Vektor


           /*Ausgabe*/

          

           //Initialize the gnuplot handle 

          printf("*** example of gnuplot control through C ***\n") ;

           h1 = gnuplot_init() ;

           //Plot der Datenpunkte 
gnuplot_set_xlabel(h1, "Dimension n der Matrix") ;
gnuplot_set_ylabel(h1, "Berechnungszeit [s]") ;

           gnuplot_plot_xy(h1, n_vec_doub, dt_vec_gauss, 6, "gauss") ;

           gnuplot_setstyle(h1,"lines");

           gnuplot_plot_xy(h1,n_vec_doub, dt_vec_LU, 6,"lusol");

           
           
           
/*gnuplot_set_xlabel(h1, "Dimension n der Matrix") ;
gnuplot_set_ylabel(h1, "Berechnungszeit [s]") ;
gnuplot_plot_xy(h1, n_plot, lusol, 6, "Zeit lusol") ;
gnuplot_plot_xy(h1, n_plot, gauss, 6, "Zeit Gauss"); */

          

    //Schließen des Plots 

           printf("press ENTER to continue\n");

           while (getchar()!='\n'){}

           gnuplot_close(h1) ; 
    
    
           
        
     return 0;
 }
 

     
