 

    // TEIL 2 Gaußelimination
    
#include <stdio.h>
#include <nmrc.h>
#include <stdlib.h>
#include <math.h>

 int main() {
     
     
     int dim;
     double *matrixvec;
     FILE *fd;
     
     
     fd = fopen("A.txt","r"); //Matrix A öffnen
     
     if (read1D_LGS(fd, &dim, &matrixvec) < 0) //LGS (matrix und vektor b) einlesen und in matrixvec schreiben
         return 1; //wenn einlesen nicht funktioniert, wird 1 ausgegeben
         
    fclose(fd); //datei schließen
    //printf("%i",dim);
    
 }
    
    
    
    int gauss_elim(int dim, double *matrixvec, int fdim, double *b, double *x){
        
        double max_Pivotelement = matrixvec[0];
        int max_StellePivotelement = 0;
        
    int k, m, Stelle, n, zeile, spalte;
    
        for ( k=0;k < dim-1;k++){ 
            
    //Pivotsuche
            for ( Stelle = 1; Stelle<=(dim*dim+dim-1); Stelle+=dim+1) {
        
                if (fabs(max_Pivotelement)<fabs(matrixvec[Stelle])) {
                    max_Pivotelement = fabs(matrixvec[Stelle]);  
                    max_StellePivotelement = Stelle;
                }}
                
              // zeile = max_StellePivotelement/(dim+1) //zeile in der das größte pivotelement steht
                
                
            
                
             //stelle an der die vertauschung der zeilen beginnt
            
            if (max_Pivotelement !=  matrixvec[0]) {
                
                 m = 0;
                
                // for ( int spalte = 0; spalte <= dim; spalte +=1) {
                for ( n =  max_StellePivotelement; n<=max_StellePivotelement+dim+1; n +=1) {   
                   // for (int m=0; m<=dim-1; m+=1) {
                    
                    int schrittweite = max_StellePivotelement - 0;
                    
                    double alterwert = matrixvec[n - schrittweite];
                    matrixvec[n - schrittweite] = matrixvec[max_StellePivotelement+m];
                    matrixvec[max_StellePivotelement] = alterwert;
                    m = m+1;
                    }
        
                    
                    
                }
                
                 //Aufdatierung
         // k = 0: in nullte spalte nuller schreiben, k = 1: in erste spalte nuller schreiben...
         //
         int breite = dim+1;
         
        for ( zeile=k+1;zeile<dim;zeile++){
            
        for ( spalte=k;spalte<dim+1;spalte++) {
            
        matrixvec[zeile*breite+spalte] = matrixvec[zeile*breite+spalte] - matrixvec[zeile*breite+k] /  matrixvec[k*breite+k] *  matrixvec[k*breite+spalte];
        }
    
       
    }
        
                    
                    
            
            //Rückwärtssubstitution
    
         mat_print("matrixvec", " %.5lff ", stdout, dim, dim, dim, matrixvec);    

        }
            
    return 0; 
    }
               
            
 
